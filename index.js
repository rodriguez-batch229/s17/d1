// [Section] - Functions
//  Functions in JS are lines/blocks of codes that will tell our device for application to perform a particular task.
//  Functions are mostly created to create complicated task to run several lines of code succession.
// They are also used to prevent the repeating line of codes.

// Function Declarations -> "function"

/* Syntax:

function functionName (){
    // code block
    let number = 10;
    console.log(number); -> local variable
}


 */

function printName() {
    console.log("My Name is John");
}

// Calling or invoke/invocation
printName();

// [Section] Function Invocation
// Invocation will execute block of codes inside function being called
// This will call function named printName()
printName();

// sampleInvocations(); --> undecleared function cannot be invoked

// [Section] Function declaration vs expression
// This is a sample of function declaration
function declaredFunction() {
    console.log("Hello World from declared function");
}
declaredFunction();

// This is function expression
// A function can also be stored in a variable. this is called function expression
// Anonymous function - a function without a name.

// variableFunction();

/* 
    error - function expressions being stored in a let or const, cannot be hoisted
 */

// Anonymous function
// Function Expression Example

let variableFunction = function () {
    console.log("Hello Again");
};
variableFunction();

let functionExpression = function funcName() {
    console.log("Hello from the other side");
};
functionExpression(); //this is the right invocatuon in function expression
// funcName(); --> This will return an error

// You can re-assign declared functions and functions expressions to a new anonymous function

declaredFunction = function () {
    console.log("Updated declaredFunction");
};
declaredFunction();

functionExpression = function () {
    console.log("Updated functionExpression");
};
functionExpression();

// howeer, we cannot re-assign a function expression initialized with const

/* 
const constantFunction = function () {
    console.log("Intializing constant function");
};
constantFunction();

constantFunction = function () {
    console.log("will try to re assign");
};
constantFunction();
 */

// [Section] - Function Scoping

/* 
1. Local/Block Scoping
2. Global Scoping
3. Function Scoping
*/

{
    // This is a local variable and its value is only accessible inside curly brackets
    let localVariable = "Armando Perez";
    console.log(localVariable);
}

// This is a global variable and its value is acessible anywhere in the code base
let globalVariable = "Mr. WorldWide";

console.log(globalVariable);
// console.log(localVariable); --> will return error

function showNames() {
    // function scoped variables
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}
showNames();

/* console.log(functionVar);
console.log(functionConst);
console.log(functionLet); 

This will return error since the 3 variables are stored in a function.

*/

// Nested function

function myNewFunction() {
    // This is a global variable inside this function
    let name = "Jane";

    function nestedFunction() {
        let nestedName = "John";
        // child function can inherit data from parent function
        console.log(name);
    }
    nestedFunction();
    // console.log(nestedName); -> error/function
}
myNewFunction();
// nestedFunction(); -> result will be error

// Function and global scope variables

let globalName = "Alexandro";

function myNewFunction2() {
    let nameInside = "Renz";
    console.log(globalName);
}

myNewFunction2();
// console.log(nameInside); -> will return an error

// [Section] - using Alert
// alert() allows us to show small window at the top of our browser

// alert("Hello, world!");

// function showSampleAlert() {
//     alert("Hello, User! ");
// }

// showSampleAlert();
// console.log("i will only log in the console when alert is dismissed.");

// notes on the use of alert()
// Show only alert for short dialog message.
// Do not overuse alert() because program/js has to wait for it to be dismissed before continuing

// [Section] - using prompt ()
// prompt() allows us to show a small window at the top of our browser to gather user input

// usually prompts are stored in a variable

// let samplePrompt = prompt("Please enter your name");

// console.log("Hello, $samplePrompt");

// document.getElementById("text").innerHTML = samplePrompt;

// let sampleNullPromp = prompt("do not enter anything");

// console.log(sampleNullPromp); //returns an empty string

function printWelcomeMessage() {
    let firstName = prompt("Please enter your first name");
    let lastName = prompt("Please enter your last name");

    console.log("Hello," + firstName + lastName);
}
printWelcomeMessage();

// [SECTION] - Function Naming Convention
// Function names should be definitive of the task it will perform. It usually contains verb.

function getCourses() {
    let courses = ["Science 101", "Math 101", "English 101"];
    console.log(courses);
}

getCourses();

// avoid generic names to avoid confusion within the code

function get() {
    let name = "Jamie";
    console.log(name);
}

get();
